###############################################################################
# Script        :   d3dCondaEnvOffline.py
# Description   :   Script to migrate a Conda Environment from a PC/server with
#                   an Internet connection to another PC/server without an
#                   Internet connection.
#                   This script will do (in thePC/server with an Internet
#                   connection):
#                       1. Access the Conda Environment to migrate:
#                           activate {my-conda-environment}
#                       2. Create a Conda Explicit Specification File from this
#                       environment:
#                           conda list --explicit > spec-file.txt
#                       3. Download all dependency packages (from the Conda
#                       Specification file) to a directory
#                       4. Create a script that it will create the Conda
#                       Environment and install all the dependencies packages
#                       in the destination PC/Server (without an
#                       Internet connection)
# Tested:           Miniconda3 - v4.5.11 (64bit)
# Author        :   David Tufet Palaci
# Email         :   davidtufet@gmail.com
# Version       :   0.1.0
# Last modified :   12/11/2018
###############################################################################


################################################################
# Imports
################################################################
import sys
import os
import argparse
import subprocess
import json
import re
import requests
from datetime import datetime


################################################################
# Constants
################################################################
MY_ARG = ['-env', '--conda-env', True,
            'Specify the name of the Conda Environment to migrate']
CMD_CONDA_ENV_LIST = "conda env list --json"
CMD_CONDA_LIST_EXPLICIT = "conda list --name {} --explicit"
CMD_CONDA_CREATE_ENV = "conda create --name {} --offline"
CMD_CONDA_ENV_ACTIVATE = "conda activate {}"
CMD_CONDA_ENV_DEACTIVATE = "conda deactivate"
CMD_CONDA_INSTALL = "conda install --offline "
MY_SPEC_FILE = "{}.spec-file.{}.txt"
MY_SCRIPT_FILE = "{}.migration-script.{}"
MY_DWNLOAD_DIR = "pkgs_downloaded_{}"


################################################################
# Functions
################################################################

def check_params():
    # Function to check the passed arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(MY_ARG[0], MY_ARG[1],
                            required=MY_ARG[2], help=MY_ARG[3])
    return parser.parse_args()


def run_command(my_cmd):
    # Function to running commands
    list_cmd = my_cmd.split(" ")
    try:
        p = subprocess.Popen(list_cmd, stdin=subprocess.PIPE,
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
    except Exception as e:
        print ("[ ERROR ] Command: {}. Message: {}.".format(my_cmd, e))
        sys.exit(1)
    else:
        stdout = stdout.decode("utf-8")
    return stdout 


def check_conda_env(name_env):
    # Function that checks if a Conda Environment exists
    stdout = run_command(CMD_CONDA_ENV_LIST)
    dict_envs = json.loads(stdout)
    is_env = False
    for my_env in dict_envs['envs']:
        my_env = my_env.replace('\\','/')
        if my_env.split('/')[-1] == name_env: is_env = True
    if not is_env:
        print ("[ ERROR ] The Conda Environment passed to the script"
                " does not exist: {}".format(name_env))


def create_directories(name_env, my_time):
    # Function to create the directories where the script will download all
    # the dependency packages and the spec-file, and where the script will
    # create the script
    script_path = os.path.dirname(
                            os.path.realpath(sys.argv[0])).replace('\\', '/')
    my_script_dir = script_path + "/" + name_env
    if not os.path.exists(my_script_dir): os.makedirs(my_script_dir)
    my_download_dir = script_path + "/" + name_env + "/" + \
                        MY_DWNLOAD_DIR.format(my_time)
    if not os.path.exists(my_download_dir): os.makedirs(my_download_dir)
    return my_script_dir, my_download_dir


def conda_list_explicit(name_env, my_dir, my_time):
    # Function to run a 
    # "conda list --name {my-conda-env} --explicit"
    stdout = run_command(CMD_CONDA_LIST_EXPLICIT.format(name_env))
    stdout = "\n".join([s for s in stdout.splitlines() if s])
    my_spec_file = my_dir + "/" + MY_SPEC_FILE.format(name_env, my_time)
    print ('> Creating the Conda Spec File: {}...'.format(my_spec_file))
    with open(my_spec_file, 'w') as f_handle:
        f_handle.write(stdout)
    print ('[ OK ]')
    return stdout


def parser_conda_spec(conda_spec):
    # Function to parser a conda specification passed as an argument and return
    # a list with all URLs (for all the dependency packages) to downloads
    my_pattern = re.compile(r'^(http.*?)$', flags=re.MULTILINE|re.DOTALL)
    list_url = [my_match.group(1) \
                for my_match in my_pattern.finditer(conda_spec)]
    return list_url


def download_packages(list_url, my_download_dir):
    # Function to download all the dependency packages
    for my_url in list_url:
        print ('> Downloading: {}...'.format(my_url))
        my_download_file = my_download_dir + "/" + my_url.split("/")[-1]
        ret = requests.get(my_url)        
        if ret.status_code == 200:
            with open(my_download_file, 'wb') as f_handle:
                f_handle.write(ret.content)
            print ('[ OK ] {}'.format(my_download_file))
        else:
            print("[ ERROR ] {}".format(ret.__str__()))


def create_script_to_install_pkgs(name_env, list_url, my_dir, my_time):
    # Function to download all the dependency packages
    my_script_file = my_dir + "/" + MY_SCRIPT_FILE.format(name_env, my_time)
    print ('> Creating the Migration Script: {}...'.format(my_script_file))
    my_script_data = CMD_CONDA_CREATE_ENV.format(name_env) + "\n" + \
                        CMD_CONDA_ENV_ACTIVATE.format(name_env) + "\n"
    for my_url in list_url:
        my_script_data = my_script_data + \
                            CMD_CONDA_INSTALL + my_url.split("/")[-1] + "\n"
    my_script_data = my_script_data + CMD_CONDA_ENV_DEACTIVATE
    with open(my_script_file, 'w') as f_handle:
        f_handle.write(my_script_data)
    print ('[ OK ]')
    

def main():
    # Function with the main
    
    my_args = check_params()
    check_conda_env(my_args.conda_env)
    my_time = datetime.now().strftime("%Y%m%d-%H%M%S")
    my_dir, my_download_dir = create_directories(my_args.conda_env, my_time)
    stdout = conda_list_explicit(my_args.conda_env, my_dir, my_time)
    list_url = parser_conda_spec(stdout)
    download_packages(list_url, my_download_dir)
    create_script_to_install_pkgs(my_args.conda_env, list_url, my_dir, my_time)



################################################################
# Main
################################################################

if __name__ == '__main__':
    main()

